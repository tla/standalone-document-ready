/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * standalone-document-ready
 * @see {@link https://framagit.org/tla/standalone-document-ready}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/standalone-document-ready/blob/master/LICENSE}
 *
 * @function domLoad
 * @return void
 * */

( function () {
    
    'use strict';
    
    var global;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    global.domLoad = ( function () {
        
        var ready = false ,
            stackfunction = [] ,
            document = global.document;
        
        function stack ( callback ) {
            if ( isfunction( callback ) ) {
                if ( ready ) {
                    return callback.call( document );
                }
                
                stackfunction.push( callback );
            }
        }
        
        function event ( element , fn , callback ) {
            [ 'DOMContentLoaded' , 'DomContentLoaded' , 'load' ].forEach( function ( event ) {
                element[ fn ]( event , callback , true );
            } );
        }
        
        function load () {
            if ( !ready ) {
                ready = true;
                
                stackfunction.forEach( function ( callback ) {
                    callback.call( document );
                } );
                
                stackfunction = null;
                
                if ( document ) {
                    event( document , 'removeEventListener' , load );
                }
                
                event( global , 'removeEventListener' , load );
            }
        }
        
        if ( document ) {
            event( document , 'addEventListener' , load );
        }
        
        event( global , 'addEventListener' , load );
        
        return stack;
        
    } )();
    
} )();